## Exercise Pair Sum Indices

**Status: Ready for use**

This exercise is designed to help practitioners improve their problem-solving and coding skills by working with lists and integers in a programming context. It encourages the use of efficient algorithms and data structures to find the desired solution pair, as well as the ability to reason about the correctness and uniqueness of the solution. Additionally, this exercise can help practitioners gain experience with common programming tasks such as iterating over lists, accessing elements by index, and comparing integers.

Given a list of integers l and a target integer n, return the indices 
(i, j) (with i != j) such that l[i] + l[j] == n

For any input, you can assume the solution (i, j) to always exist and be unique

You can return either (i, j) or (j, i) (order is not relevant)

**Example:**
Input: l = [2, 7, 11, 15] ,  n = 9
Output: [0, 1]



## Testing

```shell
npm run test
``` 

## Development

```shell
npm run dev
```