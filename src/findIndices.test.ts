import { describe } from 'riteway';
import findIndices from './findIndices';

describe('findIndices()', async assert => {
  {
    const numbers = [2, 7, 11, 15];
    const target = 9;
    const expected = [0, 1];
    assert({
      given: 'a list of numbers and a target sum',
      should: 'return the indices of two numbers that add up to the target sum',
      actual: findIndices(numbers, target),
      expected
    });
  }

  {
    const numbers = [3, 2, 4];
    const target = 6;
    const expected = [1, 2];
    assert({
      given: 'another list of numbers and a target sum',
      should: 'return the indices of two numbers that add up to the target sum',
      actual: findIndices(numbers, target),
      expected
    });
  }
});