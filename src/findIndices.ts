function findIndices(l: number[], n: number): [number, number] {
    const indexMap = new Map<number, number>();
    for (let i = 0; i < l.length; i++) {
      const number = l[i];
      if (indexMap.has(n - number)) {
        return [indexMap.get(n - number)!, i];
      }
      indexMap.set(number, i);
    }
    throw new Error('No valid pair found');
  }

export default findIndices